#demographics table for LCCC
#Kirsten Burdett 
# update Sep 28, 2018

#============= for categorical variables   ===============#
catvarfun <- function(dat, catvar, roundval, titlenam){
  freqs <- round(table(dat[,catvar], exclude = NULL),roundval)
  props <- round(prop.table(freqs)*100, roundval+1)
  val <- paste0(freqs, " (", props, ")")
  outnames2 <- names(table(dat[,catvar], exclude =NULL))
  outnames <- paste0(titlenam, "__*__",outnames2)
  
  outdat <- data.frame(outnames, val, stringsAsFactors = FALSE)
  finalo <- rbind(c(paste0(titlenam," (%)") ,""),outdat)
}


#============= for continuous variables   ===============#
contfun <- function(dat, continp, roundval,titlenam){
  contvar <- enquo(continp)
  MeanSD <- paste0(titlenam,"__*__Mean(SD)")
  medval <- paste0(titlenam,"__*__Median")
  Range <- paste0(titlenam, "__*__Range")
  
  outfirst <- dat %>%
    summarise(meannew := round(mean(!! contvar, na.rm=TRUE),roundval),
              sdnew := round(sd(!! contvar, na.rm=TRUE),roundval+1),
              !!medval := round(median(!! contvar, na.rm=TRUE),roundval),
              minnew := round(min(!! contvar, na.rm=TRUE),roundval),
              maxnew := round(max(!! contvar, na.rm=TRUE),roundval)) %>%
    mutate(!!MeanSD := paste0(meannew," (",sdnew,")"), !!Range := paste0("(", minnew,"-",maxnew,")")) %>%
    select(!!medval, !!Range) #removed mean(sd) since no need to show
  
  # here we add the row of names and the contvar name
  val <- t(outfirst)
  outnames <- names(outfirst)
  newd <- data.frame(outnames, val, stringsAsFactors = FALSE)
  nameval <- c(titlenam, "")
  final <- rbind(nameval, newd)
  
  return(final)
}
